Тестовое приложение для DoubletApp - "Трекер привычек"

Цель: показ данных с сервера в виде таблицы с возможностью модификации объектов как в отдельном VC (добавление/изменение), так и свайпами строк (удаление и "выполнение"). Также реализована фильтрация и сортировка списка.

API: https://droid-test-server.doubletapp.ru/swagger/index.html 

Stack:
Swift
UIKit
Alamofire
Realm
Codable
NotificationToast

Architecture: Model-View-Controller
