
import UIKit
import NotificationToast

class HabitsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - properties
    @IBOutlet weak var sortButton: UIButton! // кнопка сортировки
    @IBOutlet weak var searchBar: UISearchBar! // строка поиска (фильтрации)
    @IBOutlet weak var tableView: UITableView! // таблица
    
    private var provider = DataProvider() // провайдер, управляющий данными
    var habits: [Habit] = [] // массив для заполнения таблицы
    var isSorted = true // свойство для сортировки
    var filteredHabits = [Habit]() // массив для фильтрации
    let indicator = UIActivityIndicatorView(style: .medium) // индикатор загрузки контента
    
    // переменная, обозначающая какой таб выбран в данный момент
    var isGoodHabits: Bool {
        return tabBarController?.selectedIndex == 0 ? true : false
    }
    
    // функция для отображения корректного тайтла для navigationBar и tabBar
    func setTitles() {
        if isGoodHabits {
            self.title = "Хорошие привычки"
        } else {
            self.title = "Плохие привычки"
        }
        
        tabBarController?.tabBar.items?[0].title = "Хорошие"
        tabBarController?.tabBar.items?[1].title = "Плохие"
        
        tabBarController?.tabBar.items?[0].image = UIImage(systemName: "plus.circle.fill")
        tabBarController?.tabBar.items?[1].image = UIImage(systemName: "minus.circle.fill")
    }
    
    // MARK: - tableView init
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return habits.count
    }
    
    // инициализация строк
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? HabitTableViewCell
        
        guard let cell = c else { return UITableViewCell() }
        
        let habit = habits[indexPath.row]
        cell.fillData(from: habit, isGoodHabits)
        
        return cell
    }
    
    // MARK: - tableView swipe actions
    // инициализация двух свайп-экшнов
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // кнопка Done - привычка выполнена 1 раз
        let addAction = UIContextualAction(style: .normal, title: "Done") { (action, view, completion) in
            let habit = self.habits[indexPath.row]
            let h = habit.done()
            
            // запускаем функцию выполнения привычки
            self.provider.addOrUpdateData(newHabit: h, completion: { [weak self] success in
                
                var str = "" // текст для алерта
                
                // в случае успешного получения ответа от сервера
                if success {
                    let h = habit.count
                    
                    guard let isGoodHabits = self?.isGoodHabits else {return}
                    
                    // инициализируем текст для алерт контроллера
                    if h > 0 {
                        str = isGoodHabits ? "Стоит выполнить еще \(h) раз" : "Можете выполнить еще \(h) раз"
                    } else if h < 0 {
                        str = isGoodHabits ? "You are breathtaking!" : "Хватит это делать!"
                    } else {
                        str = isGoodHabits ? "Поздравляю! Вы добились своей цели!" : "Вы исчерпали лимит!"
                    }
                    self?.tableView.reloadData()
                    
                } else { // в случае неуспеха показываем ошибку
                    str = "Ошибка сети"
                }
                // инициализируем тост
                let toast = ToastView(
                    title: str,
                    titleFont: .systemFont(ofSize: 13, weight: .regular),
                    onTap: { print("Tapped!") }
                )
                toast.displayTime = 3
                toast.show()
                completion(true)
            })
        }
        
        // кнопка удалить строку
        let deleteAction = UIContextualAction(style: .normal, title: "Delete") { (action, view, completion) in
            
            let habit = self.habits[indexPath.row]
            
            // удаляем строку через провайдер
            self.provider.removeData(removedHabit: habit, completion: { [weak self] success in
                
                if success {
                    // после удаления на сервере и обновления БД - удаляем из таблицы
                    self?.habits.remove(at: indexPath.row)
                    self?.tableView.reloadData()
                    completion(true)
                    
                } else  { // в случае неуспешного удаления на сервере показываем тост
                    let toast = ToastView(
                        title: "Ошибка сети",
                        titleFont: .systemFont(ofSize: 13, weight: .regular),
                        onTap: { print("Tapped!") }
                    )
                    toast.displayTime = 3
                    toast.show()
                    completion(true)
                }
            })
        }
        addAction.backgroundColor = .green
        deleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [deleteAction, addAction])
    }
    
    // MARK: - tableView selection
    // реализуем возможность тапа по строке для дальнейшего редактирования
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let habit = habits[indexPath.row]
        
        // инициализируем второй вью контроллер
        guard let vc = storyboard?.instantiateViewController(identifier: "NewHabitViewController") as? NewHabitViewController else {return}
        
        // присваиваем свойствам инициализируемого контроллера значения выбранной строки
        vc.uid = habit.uid
        vc.titleText = habit.title
        vc.descriptionText = habit.descr
        vc.priority = habit.priority
        vc.type = habit.type
        vc.frequencyText = "\(habit.frequency)"
        vc.countText = "\(habit.count)"
        vc.date = habit.date
        vc.buttonTitle = "Сохранить"
        
        // открываем вью контроллер для редактирования записи
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setTitles() // устанавливаем тайтлы
        
        // назначаем наблюдателей клавиатуры для сдвига вью перед вводом текста
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // добавляем индикатор на вью и анимируем до получения контента
        view.addSubview(indicator)
        indicator.center = view.center
        indicator.startAnimating()
        
        // получаем данные с сервера
        provider.fetchData(completion: { [weak self] habits in
                        
            // проверяем массив на nil
            guard let habits = habits else {return}
            
            // отбираем данные по типу
            self?.habits = self?.isGoodHabits ?? true ?
                habits.filter({$0.type == 0}) :
                habits.filter({$0.type == 1})
            
            // заполняем данные для фильтрации
            self?.filteredHabits = self?.habits ?? []
            self?.searchBar.text = ""
            self?.searchBar.delegate = self
            
            // высота строки в зависимости от высоты контента
            self?.tableView.rowHeight = UITableView.automaticDimension
            
            // останавливаем индикатор после получения данных
            self?.indicator.stopAnimating()
            
            self?.tableView.reloadData()
        })
    }
    
    // MARK: - viewDidDisappear
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        indicator.stopAnimating() // останавливаем индикатор (при ошибке запроса с сервера)
        
        NotificationCenter.default.removeObserver(self) // удаляем наблюдателей
    }
    
    // MARK: - sort table
    // сортировка таблицы
    @IBAction func sortTable(_ sender: Any) {
        let sortedHabits = habits
        // сортируем таблицу по дате и меняем image кнопки
        if isSorted {
            sortButton.setImage(UIImage(systemName: "arrow.up"), for: .normal)
            self.habits = sortedHabits.sorted() {$0.date > $1.date}
        } else {
            sortButton.setImage(UIImage(systemName: "arrow.down"), for: .normal)
            self.habits = sortedHabits.sorted() {
                $0.date < $1.date
            }
        }
        isSorted = !isSorted
        
        tableView.reloadData()
    }
    
    // MARK: - filter table
    // строка поиска
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // если строка пустая (вариант при стирании символов) - то вернуть обратно первоначальный массив
        if searchBar.text?.count == 0 {
            habits = filteredHabits
        } else {
            // фильтруем относительно введенного текста
            habits = filteredHabits.filter() {
                $0.title.lowercased().contains(searchText.lowercased())
            }
        }
        tableView.reloadData()
    }
    
    // функция для скрытия клавиатуры поиска по кнопке Search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: - keyboard show/hide
    // отобразить клавиатуру со сдвигом вью
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableView.contentInset.bottom = keyboardHeight // поднимаем нижний край вью до верхней границы клавиатуры
        }
    }
    // возвращаем нижний край вью на дефолтное значение
    @objc func keyboardWillHide(notification: NSNotification) {
        tableView.contentInset.bottom = 0.0
    }
}
