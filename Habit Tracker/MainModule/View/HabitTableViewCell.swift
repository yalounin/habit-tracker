
import UIKit

// ui-элементы кастомной строки
class HabitTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var descr: UILabel!
    @IBOutlet weak var frequency: UILabel!
    @IBOutlet weak var priority: UIView!
    
    // функция заполнения строки данными
    func fillData(from habit: Habit, _ isGoodHabits: Bool) {
        title.text = habit.title
        descr.text = habit.descr
        
        // так как мы фильтруем массив по типу, то для каждого таба свой текст
        type.text = isGoodHabits ? "Хорошая привычка" : "Плохая привычка"
        
        // данные счетчика в строке
        switch habit.count {
        case 2, 3, 4:
            frequency.text = isGoodHabits ? "Осталось \(habit.count) раза за \(habit.frequency) дней" : "Можно еще выполнить \(habit.count) раза за \(habit.frequency) дней"
        case Int.min ... 0:
            frequency.text = isGoodHabits ? "Вы выполнили цель!" : "Вы исчерпали лимит!"
        default:
            frequency.text = isGoodHabits ? "Осталось \(habit.count) раз за \(habit.frequency) дней" : "Можно еще выполнить \(habit.count) раз за \(habit.frequency) дней"
        }
        // в зависимости от приоритета, выделяем вью соответствующим цветом
        let priority = Priorities(rawValue: habit.priority)
        self.priority.backgroundColor = priority?.color
    }
}
