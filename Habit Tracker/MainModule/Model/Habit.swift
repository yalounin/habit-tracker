

import Foundation
import RealmSwift

// класс для работы с массивом данных в БД
class HabitList: Object {
    @objc dynamic var id = ""
    dynamic var habits: List<Habit> = List()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

// объединенная модель для Realm и Codable
class Habit: Object, Codable {
    @objc dynamic var uid = ""
    @objc dynamic var title = ""
    @objc dynamic var type = 0
    @objc dynamic var descr = ""
    @objc dynamic var priority = 0
    @objc dynamic var frequency = 0
    @objc dynamic var count = 0
    @objc dynamic var date = 0
    
    override class func primaryKey() -> String? {
        return "uid"
    }
    
    // кодовые ключи
    private enum CodingKeys: String, CodingKey {
        case uid
        case title
        case type
        case descr = "description"
        case priority
        case frequency
        case count
        case date
    }
    
    // кодеры/декодеры
    internal func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(uid, forKey: .uid)
        try container.encode(title, forKey: .title)
        try container.encode(type, forKey: .type)
        try container.encode(descr, forKey: .descr)
        try container.encode(priority, forKey: .priority)
        try container.encode(frequency, forKey: .frequency)
        try container.encode(count, forKey: .count)
        try container.encode(date, forKey: .date)
    }
    
    internal func decode(to decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        uid = try container.decode(String.self, forKey: .uid)
        title = try container.decode(String.self, forKey: .title)
        type =  try container.decode(Int.self, forKey: .type)
        descr = try container.decode(String.self, forKey: .descr)
        priority = try container.decode(Int.self, forKey: .priority)
        frequency = try container.decode(Int.self, forKey: .frequency)
        count = try container.decode(Int.self, forKey: .count)
        date = try container.decode(Int.self, forKey: .date)
    }
    
    // функция, возвращающая копию объекта, но с уменьшенным на 1 счетчиком
    func done() -> Habit {
        let habit = Habit()
        habit.uid = uid
        habit.title = title
        habit.descr = descr
        habit.frequency = frequency
        habit.type = type
        habit.priority = priority
        habit.count = count - 1
        habit.date = Int(Date().timeIntervalSince1970)
        return habit
    }
}




