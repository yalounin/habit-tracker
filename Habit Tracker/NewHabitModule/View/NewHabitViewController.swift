

import UIKit
import NotificationToast

// экран добавления/редактирования объекта
class NewHabitViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    private let provider = DataProvider() // объявляем провайдера
    
    // MARK: - properties
    // ui элементы
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var descriptionTF: UITextField!
    @IBOutlet weak var frequencyTF: UITextField!
    @IBOutlet weak var countTF: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var priorityPicker: UIPickerView!
    @IBOutlet weak var goodTypeButton: UIButton!
    @IBOutlet weak var badTypeButton: UIButton!
    
    // свойства, необходимые для сохранения данных
    var uid = ""
    var titleText = ""
    var descriptionText = ""
    var priority = 0
    var type = 0
    var frequencyText = ""
    var countText = ""
    var buttonTitle = "Добавить" // тайтл кнопки по дефолту
    var date = 0
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 // количество компонентов пикера
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Priorities.allCases.count // количество строк (по количеству уровней приоритета)
    }
    
    // заполняем пикер данными из энума
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let priority = Priorities(rawValue: row)
        return priority?.descr
    }
    
    // MARK: - addHabit button
    // добавить элемент
    @IBAction func addHabit(_ sender: Any) {
        
        // инициализируем объект habit
        let habit = Habit()
        habit.uid = uid
        habit.title = titleTF.text ?? ""
        habit.descr = descriptionTF.text ?? ""
        habit.priority = priorityPicker.selectedRow(inComponent: 0) // выбранный компонент пикера
        habit.type = type
        habit.frequency = Int(frequencyTF.text ?? "") ?? 0
        habit.count = Int(countTF.text ?? "") ?? 0
        habit.date = Int(Date().timeIntervalSince1970) // текущая дата (согласно бэкенд-модели дата обновляемого объекта должна быть больше)
        
        // если текстовые поля пустые
        if titleTF.text == "" || descriptionTF.text == "" || frequencyTF.text == "" || countTF.text == "" {
            let toast = ToastView(
                title: "Ошибка",
                titleFont: .systemFont(ofSize: 13, weight: .regular),
                subtitle: "Необходимо заполнить все поля",
                subtitleFont: .systemFont(ofSize: 11, weight: .light),
                onTap: { print("Tapped!") }
            )
            toast.displayTime = 3
            toast.show()
            return
        }
        
        // если дата и количество не положительные
        if habit.frequency <= 0 || habit.count <= 0 {
            let toast = ToastView(
                title: "Ошибка",
                titleFont: .systemFont(ofSize: 13, weight: .regular),
                subtitle: "Период и количество должны быть положительными",
                subtitleFont: .systemFont(ofSize: 10, weight: .light),
                onTap: { print("Tapped!") }
            )
            toast.displayTime = 3
            toast.show()
            return
        }
        
        // Отображаем индикатор загрузки вместо кнопки
        self.saveButton.isHidden = true
        let indicator = UIActivityIndicatorView(style: .medium)
        view.addSubview(indicator)
        indicator.center = saveButton.center
        indicator.startAnimating()
        
        // добавляем/обновляем данные через провайдер
        provider.addOrUpdateData(newHabit: habit, completion: { [weak self] success in
            if success {
                indicator.stopAnimating() // приостанавливаем индикатор
                // закрываем вью контроллер
                self?.navigationController?.popViewController(animated: true)
            } else { // при неуспешном запросе на сервер возвращаем кнопку и показываем тост
                self?.saveButton.isHidden = false
                indicator.stopAnimating()
                let toast = ToastView(
                    title: "Ошибка сети",
                    titleFont: .systemFont(ofSize: 13, weight: .regular),
                    onTap: { print("Tapped!") }
                )
                toast.displayTime = 3
                toast.show()
            }
        })
    }
    
    // MARK: - viewDidLoad()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // назначаем наблюдателей клавиатуры для сдвига вью перед вводом текста
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // скрыть клавиатуру при тапе по вью
        view.addGestureRecognizer(UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:))))
        
        // отображаем в текстовых полях пропертис (для редактирования существующего объекта)
        titleTF.text = titleText
        descriptionTF.text = descriptionText
        priorityPicker.selectRow(priority, inComponent: 0, animated: true) // выбираем нужную строку пикера
        frequencyTF.text = frequencyText
        countTF.text = countText
        saveButton.setTitle(buttonTitle, for: .normal) // присваиваем тайтл кнопке
        typeButtonTapped(nil, type: type) // выделяем нужную кнопку для отображения типа привычки
        
        // назначаем вью делегатом текстовых полей для скролла при показе таблицы
        titleTF.delegate = self
        descriptionTF.delegate = self
        
        // MARK: - type buttons init
        // добавляем таргет для кнопок типа
        goodTypeButton.addTarget(self, action: #selector(typeButtonTapped), for: .touchUpInside)
        badTypeButton.addTarget(self, action: #selector(typeButtonTapped), for: .touchUpInside)
    }
    
    // подготовка данных для создания и редактирования объекта / отображение типа существующего объекта
    @objc func typeButtonTapped(_ sender: UIButton?, type: Int) {
        goodTypeButton.layer.borderWidth = 1
        badTypeButton.layer.borderWidth = 1
        
        if sender == goodTypeButton || type == 0 {
            goodTypeButton.layer.borderColor = #colorLiteral(red: 0.6509803922, green: 0.1254901961, blue: 0.1490196078, alpha: 1)
            goodTypeButton.layer.borderWidth = 2
            badTypeButton.layer.borderColor = UIColor.lightGray.cgColor
            self.type = 0
        } else if sender == badTypeButton || type == 1 {
            badTypeButton.layer.borderColor = #colorLiteral(red: 0.6509803922, green: 0.1254901961, blue: 0.1490196078, alpha: 1)
            goodTypeButton.layer.borderColor = UIColor.lightGray.cgColor
            badTypeButton.layer.borderWidth = 2
            self.type = 1
        }
    }
    
    // MARK: - keyboard show/hide
    // отобразить клавиатуру со сдвигом вью при выделении нижних текстовых полей
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            // для сдвига должны выполниться как минимум 2 условия
            if self.view.frame.origin.y == 0 && (self.countTF.isEditing || self.frequencyTF.isEditing)  {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
        // приподнимаем нижний край вью
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            scrollView.contentInset.bottom = keyboardHeight
        }
    }
    
    // возвращаем вью в исходное положение при скрытии клавиатуры
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset.bottom = 0.0 // возвращаем нижний край вью на дефолтное значение
        
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

// MARK: - priorities enum
// виды приоритетов с русским описанием и цветом
enum Priorities: Int, CaseIterable {
    case low = 0
    case medium = 1
    case high = 2
    
    var descr: String {
        switch self {
        case .low: return "Низкий"
        case .medium: return "Средний"
        case .high: return "Высокий"
        }
    }
    
    var color: UIColor {
        switch self {
        case .low: return .green
        case .medium: return .yellow
        case .high: return .red
        }
    }
}


