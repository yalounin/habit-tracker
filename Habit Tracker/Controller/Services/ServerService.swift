

import Foundation
import Alamofire

// класс для работы с сервером
class ServerService {
    
    // MARK: - PROPERTIES
    // данные для создания запроса
    private let url = "https://droid-test-server.doubletapp.ru/api/habit"
    private let token = "a3e44bd2-e527-4c25-afee-2fcd938e0400"
    private let accept = "application/json"
    private var headers: HTTPHeaders {
        return ["Accept": accept, "Authorization": token]
    }
    
    // MARK: - GET DATA
    // получить весь массив данных по запросу
    func fetchHabits(completion: (([Habit]?) -> Void)?) {
        
        AF.request(url,
                   method: .get,
                   parameters: nil,
                   encoding: JSONEncoding.default,
                   headers: headers).responseJSON { response in
                    // если запрос выдает ошибку или не содержит данных вовсе, то в комплишн передаем nil
                    guard let resp = response.response, resp.statusCode < 300 else {
                        completion?(nil)
                        return
                    }
                    // декодируем полученные данные с сервера
                    guard let data = response.data else {return}
                    let habits = try! JSONDecoder().decode([Habit].self, from: data)
                    completion?(habits)
                   }
    }
    
    // MARK: - UPDATE DATA
    // добавить или обновить объект
    func addOrUpdateHabit(habit: Habit, completion: @escaping (Bool) -> Void) {
        
        // кодируем объект в json
        let jsonData = try! JSONEncoder().encode(habit)
        let params = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any]
        
        AF.request(url,
                   method: .put,
                   parameters: params,
                   encoding: JSONEncoding.default,
                   headers: headers).responseJSON(completionHandler: { response in
                    // если сервер выдает ошибку или ответа нет, то передаем false
                    guard let resp = response.response, resp.statusCode < 300 else {
                        completion(false)
                        return
                    }
                    completion(true)
                   })
    }
    
    // MARK: - DELETE DATA
    // удалить объект
    func deleteHabit(habit: Habit, completion: @escaping (Bool) -> Void) {
        
        // задаем переменную и присваиваем ей uid для удаления на сервере
        let uid = habit.uid
        
        // формируем данные для тела запроса с uid удаляемого объекта
        guard let uidData = "{ \"uid\": \"\(uid)\"}".data(using: .utf8) else {return}
        let params = try! JSONSerialization.jsonObject(with: uidData, options: .allowFragments) as? [String: Any]
        
        // отправляем данные на сервер
        AF.request(url,
                   method: .delete,
                   parameters: params,
                   encoding: JSONEncoding.default,
                   headers: headers).responseJSON(completionHandler: { response in
                    
                    // если сервер выдает ошибку или ответа нет, то передаем false
                    guard let resp = response.response, resp.statusCode < 300 else {
                        completion(false)
                        return
                    }
                    completion(true)
                   })
    }
}
