

import Foundation
import RealmSwift

// класс для работы с локальной базой данных
class DBService {
    // инициализируем объект БД
    let realm = try! Realm()
    
    // MARK: Server -> DB
    // функция для сохранения данных с сервера в БД
    func cache(_ habits: [Habit]) {
        
        // создаем переменную, содержащую массив в проперти
        let habitList =  HabitList()
        
        // создаем realm-транзакцию
        try! realm.write {
            
            // наполняем массив полученными данными
            for habit in habits {
                habitList.habits.append(habit)
            }
            // удаляем все из БД
            realm.deleteAll()
            
            // добавляем объект с массивом
            realm.add(habitList, update: .all)
        }
    }
    
    // MARK: DB -> UI
    // получить данные из БД для показа в UI
    func getObjects() -> [Habit] {
        
        // так как эта функция всегда вызывается после cache - значит в БД точно есть объект HabitList
        let habitList = realm.objects(HabitList.self).first ?? HabitList()
        
        // создаем пустой массив
        var habits = [Habit]()
        
        // наполняем массив данными из БД
        for habit in habitList.habits {
            habits.append(habit)
        }
        // возвращаем полученный массив
        return habits
    }
}
