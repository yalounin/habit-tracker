

import Foundation

// класс для взаимодействия Сервер - БД
class DataProvider {
    // инициализируем объекты сервисов
    private let serverService = ServerService()
    private let dbService = DBService()
    
    // MARK: - FETCH DATA
    // получить кэшированные данные
    func fetchData(completion: (([Habit]?) -> Void)?) {
        // запрашиваем данные с сервера
        serverService.fetchHabits(completion: { [weak self] habits in
            // если с сервера получена ошибка или данных нет, то на UI передаем nil
            guard let habits = habits else {
                completion?(nil)
                return
            }
            // кэшируем полученный массив
            self?.dbService.cache(habits)
            
            // полученный из БД массив
            let cachedHabits = self?.dbService.getObjects()
            
            // выводим результат в основном потоке через комплишн
            DispatchQueue.main.async {
                completion?(cachedHabits)
            }
        })
    }
    
    // MARK: - ADD/UPDATE DATA
    // создать или обновить объект
    func addOrUpdateData(newHabit: Habit, completion: ((_ success: Bool) -> Void)?) {
        // добавляем объект на сервер
        self.serverService.addOrUpdateHabit(habit: newHabit, completion: { [weak self] success in
            // получаем весь массив данных и кэшируем
            self?.fetchData(completion: { _ in
                completion?(success) // результат HTTP-запроса передаем дальше в комплишн
            })
        })
    }
    
    // MARK: - REMOVE DATA
    // удаление объекта
    func removeData(removedHabit: Habit, completion: ((Bool) -> Void)?) {
        // удаляем объект на сервере
        self.serverService.deleteHabit(habit: removedHabit, completion: { [weak self] success in
            // получаем весь массив данных и кэшируем
            self?.fetchData(completion: { _ in
                completion?(success)
            })
        })
    }
}

